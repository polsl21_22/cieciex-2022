# Cieciex CMS

<div style="display: flex;justify-content: center"><img style="width: 100px" src="src/utils/svg/logo/1/logo-color.svg?raw=true" alt="Cieciex logo"/>
</div>

## What is the use of this Repo
This Project is a CMS for Cieciex application

##### As System Admin:
1. CRUD on all users
2. View all parkings
3. Add new parking 
4. Edit existing parkings
5. Assign parking to new manager
6. Browse created reservations

##### As Parking Manager:
1. View all assigned parkings
2. Edit assigned parkings
3. Browse created reservstions

## Live Application URL

##### https://cieciex.netlify.app
This URL has the application deployed in

## Gallery

<details><summary>Login</summary>
<img style="width: 100px" src="src/utils/png/screenshots/login.png?raw=true" alt="Login page"/>
</details>

<details><summary>Register</summary>
<img style="width: 100px" src="src/utils/png/screenshots/register.png?raw=true" alt="Register page"/>
</details>

<details><summary>Statistics</summary>
<img style="width: 100px" src="src/utils/png/screenshots/statistics.png?raw=true" alt="Statistics View"/>
</details>

<details><summary>Map</summary>
<img style="width: 100px" src="src/utils/png/screenshots/map.png?raw=true" alt="Location Map"/>
</details>

<details><summary>New Parking</summary>
<img style="width: 100px" src="src/utils/png/screenshots/new_parking.png?raw=true" alt="New Parking Form"/>
</details>

<details><summary>Users</summary>
<img style="width: 100px" src="src/utils/png/screenshots/user.png?raw=true" alt="Users list with single user details"/>
</details>

<details><summary>Edit User</summary>
<img style="width: 100px" src="src/utils/png/screenshots/edit_user.png?raw=true" alt="Edit user form"/>
</details>

## Used technologies and libraries

1. [React](https://reactjs.org) Library with [Typescript](https://www.typescriptlang.org)
2. [MUI](https://mui.com) for components
3. [Redux](https://redux.js.org) for state menagement
4. [RTK](https://redux-toolkit.js.org) for API calls
5. [OpenStreetMap](https://www.openstreetmap.org/) with [leaflet](https://leafletjs.com) as map provider
6. [i18next](https://www.i18next.com) to menage languages
7. [React-hook-form](https://react-hook-form.com) to create forms with [yup](https://github.com/jquense/yup) resolver

## Prerequisites

### Install Node JS
Refer to https://nodejs.org/en/ to install nodejs

### Install create-react-app
Install create-react-app npm package globally. This will help to easily run the project and also build the source files easily. Use the following command to install create-react-app

```bash
npm install -g create-react-app
```

## Cloning and Running the Application in local

Clone the project into local

Install all the npm packages. Go into the project folder and type the following command to install all npm packages

```bash
npm install
```

In order to run the application Type the following command

```bash
npm start
```

The Application Runs on **localhost:3000**
