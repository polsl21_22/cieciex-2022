import { styled } from '@mui/material'

export const PageWrapper = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100vh',
})

export const Container = styled('div')({
  boxSizing: 'border-box',
  backgroundColor: '#FFFFFF',
  height: '80%',
  width: '80%',
  border: '5px solid #9893DA',
  borderRadius: '15px',
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
})

const Column = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
})

export const LeftContainer = styled(Column)({})
export const RightContainer = styled(Column)({
  gap: '6%',
  maxHeight: '75vh',
})

export const Logo = styled('img')({
  width: '80%',
  marginBottom: '5%',
})

export const Title = styled('p')({
  textAlign: 'center',
  fontSize: 'calc(min(2.5vh, 25px))',
  maxHeight: '5vh',
})
export const Name = styled('p')({
  fontSize: '18px',
  fontWeight: '600',
})

export const Phoneimage = styled('img')({
  height: '40vh',
})

export const BottomWrapper = styled('div')({
  height: '15vh',
  justifyContent: 'space-between',
})

export const DownloadButton = styled('img')({
  maxHeight: '5vh',
})

export const MobileWrapper = styled('div')({
  display: 'flex',
  width: '100%',
  maxHeight: '12vh',
  justifyContent: 'center',
  gap: '5%',
})
