import { useLocation } from 'react-router-dom'

import { logo, mobile, phone } from 'utils'

import { DownloadPageState } from 'typings'

import {
  BottomWrapper,
  Container,
  DownloadButton,
  LeftContainer,
  Logo,
  MobileWrapper,
  Name,
  PageWrapper,
  Phoneimage,
  RightContainer,
  Title,
} from './Download.style'

const Download = () => {
  const location = useLocation()
  const state = location.state as DownloadPageState | undefined

  return (
    <PageWrapper>
      <Container>
        <LeftContainer>
          <Logo src={logo.def.color} />
          {state && state.name ? (
            <>
              <Title>Thank you for your registration</Title>
              <Name>{state.name}!</Name>
            </>
          ) : (
            <></>
          )}
        </LeftContainer>
        <RightContainer>
          <Phoneimage src={phone} />
          <BottomWrapper>
            <Title>
              Download our app now and park with no worries from today!
            </Title>
            <MobileWrapper>
              <DownloadButton src={mobile.android.white} />
              <DownloadButton src={mobile.ios.def.white} />
            </MobileWrapper>
          </BottomWrapper>
        </RightContainer>
      </Container>
    </PageWrapper>
  )
}
export default Download
