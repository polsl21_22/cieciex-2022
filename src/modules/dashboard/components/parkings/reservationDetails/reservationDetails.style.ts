import { styled } from '@mui/material'

export const DetailsContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'start',
  minWidth: '80%',
})
export const Details = styled('div')({
  display: 'flex',
  flexDirection: 'row',
})

export const DetailsLabel = styled('p')({
  fontSize: 'calc(min(1.4vh, 14px))',
  color: '#708090',
  marginRight: '1vw',
})
export const DetailsValue = styled('p')({
  fontSize: 'calc(min(1.4vh, 14px))',
  maxHeight: 'calc(min(1.4vh, 14px))',
})
