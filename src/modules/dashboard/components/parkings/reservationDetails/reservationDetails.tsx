import { useTranslation } from 'react-i18next'

import { Check, Remove } from '@mui/icons-material'
import { Button } from '@mui/material'
import moment from 'moment'

import { useGetUserByIdQuery } from 'api'

import { Reservation } from 'typings'

import {
  Details,
  DetailsContainer,
  DetailsLabel,
  DetailsValue,
} from './reservationDetails.style'

const ReservationDetails = (props: { reservation: Reservation }) => {
  const { reservation } = props
  const { t } = useTranslation()
  const { data } = useGetUserByIdQuery(reservation.user_id)
  const Detail = (props: {
    label: string
    value: string | number | JSX.Element | null
  }) => {
    return (
      <Details>
        <DetailsLabel>{props.label}</DetailsLabel>
        <DetailsValue>
          {props.value ?? (
            <Remove fontSize="small" sx={{ maxHeight: '17px' }} />
          )}
        </DetailsValue>
      </Details>
    )
  }
  return (
    <DetailsContainer>
      <Detail label={t('details.id')} value={reservation.id} />
      <Detail label={t('details.plate')} value={reservation.plate} />
      <Detail
        label={t('details.user')}
        value={`${data?.id}. ${data?.name} ${data?.surname}`}
      />
      <Detail
        label={t('details.time')}
        value={
          reservation.reserved_from && reservation.reserved_to
            ? moment(reservation.reserved_from).format('DD/MM/YYYY HH:mm') +
              ' - ' +
              moment(reservation.reserved_to).format('DD/MM/YYYY HH:mm')
            : null
        }
      />
      <Detail
        label={t('details.created')}
        value={moment(reservation.created_at).format('DD/MM/YYYY HH:mm')}
      />
      <Detail
        label={t('details.last_left')}
        value={
          reservation.last_left
            ? moment(reservation.last_left).format('DD/MM/YYYY HH:mm')
            : null
        }
      />
      <Detail
        label={t('details.is_inside')}
        value={
          reservation.is_inside ? (
            <Check fontSize="small" sx={{ maxHeight: '17px' }} />
          ) : null
        }
      />
      <Detail
        label={t('details.amount_paid')}
        value={reservation.amount_paid}
      />
      <Detail
        label={t('details.net_recived')}
        value={reservation.net_received}
      />
      <Detail
        label={t('details.payment_status')}
        value={reservation.payment_status}
      />
      <Detail
        label={t('details.excess_payment')}
        value={reservation.excess_payment}
      />
      <Detail
        label={t('details.receipt_URL')}
        value={
          reservation.receipt_URL ? (
            <Button
              size="small"
              sx={{ padding: 0, margin: 0 }}
              href={reservation.receipt_URL}
            >
              {t('details.link')}
            </Button>
          ) : null
        }
      />
    </DetailsContainer>
  )
}
export default ReservationDetails
