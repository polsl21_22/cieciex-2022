import { PageState, Parking, Reservation } from 'typings'

import ParkingAccordion from '../parkingAccordion'

const ParkingsTable = (props: {
  parkings: Parking[] | undefined
  selected: Parking | null
  isLoading: boolean
  handler: (parking: Parking) => void
  reservationHandler: (reservation: Reservation) => void
  togglePageState: (newState: PageState) => void
}) => {
  const {
    parkings,
    selected,
    isLoading,
    handler,
    reservationHandler,
    togglePageState,
  } = props
  return (
    <>
      {!isLoading &&
        parkings &&
        parkings.map(par => {
          return (
            <ParkingAccordion
              parking={par}
              selectedParking={selected}
              handleParkingChange={handler}
              reservationHandler={reservationHandler}
              key={par.id}
              togglePageState={togglePageState}
            />
          )
        })}
    </>
  )
}
export default ParkingsTable
