import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import { skipToken } from '@reduxjs/toolkit/dist/query/react'

import { useGetReservationsQuery } from 'api'

import { Parking } from 'typings'

import {
  allWeek,
  allWeekType,
  weekDays,
  weekDaysType,
} from './parkingStatistics.types'

const useParkingStatis = (selected: Parking) => {
  const { t } = useTranslation()
  const { data, isLoading } = useGetReservationsQuery(
    selected?.server_URL ?? skipToken
  )

  const [choosenWeekDay, setChoosenWeekDay] = useState<allWeekType>(allWeek[0])

  const formatStatistics = (day: weekDaysType) => {
    const stat = data?.lastWeekStatistics[day].reduce((a, b) => a + b, 0)
    return data ? stat ?? 0 : 0
  }

  const formattedData = () => {
    return weekDays.map(day => formatStatistics(day))
  }

  const barChartOptions = {
    dataLabels: {
      enabled: false,
    },
    xaxis: {
      categories: [
        t('week.monday'),
        t('week.tuesday'),
        t('week.wednesday'),
        t('week.thursday'),
        t('week.friday'),
        t('week.saturday'),
        t('week.sunday'),
      ],
    },
  }

  const barChartSeries = [
    {
      name: t('chart.hours'),
      data: data ? formattedData() : [],
    },
  ]

  const barChartOptionsWeek = (day: allWeekType) =>
    day === allWeek[0]
      ? barChartOptions
      : {
          dataLabels: {
            enabled: false,
          },
          xaxis: {
            categories: Array.from(Array(24).keys()).map(item => item + ':00'),
          },
        }

  const barChartSeriesWeek = (day: allWeekType) =>
    day === allWeek[0]
      ? barChartSeries
      : [
          {
            name: t('chart.count'),
            data: data ? data.lastWeekStatistics[day] : [],
          },
        ]

  const fontSize = { fontSize: '14' }

  const common = {
    area: 'origin',
    dataLabels: {
      enabled: false,
    },
    legend: {
      show: false,
    },
  }

  const labels = {
    name: { ...fontSize, show: true },
    value: { ...fontSize, show: true },
  }

  const donutChartOptions = {
    ...common,
    labels: [t('chart.paid'), t('chart.recived')],
    plotOptions: {
      pie: {
        startAngle: -100,
        endAngle: 100,
        donut: {
          labels: {
            show: true,
            ...labels,
            total: {
              ...fontSize,
              color: '#a024b4',
              show: true,
              label: t('chart.total_amount'),
            },
          },
        },
      },
    },
  }
  const donutChartSeries = data
    ? [
        parseFloat(
          (
            data.amountUsersPaidLastWeek - data.netValueReceivedLastWeek
          ).toFixed(2)
        ) ?? 0,
        parseFloat(data.amountUsersPaidLastWeek.toFixed(2)) ?? 0,
      ]
    : [0, 0]

  const radialBarChartOptions = {
    ...common,
    plotOptions: {
      radialBar: {
        startAngle: -100,
        endAngle: 100,
        dataLabels: {
          ...labels,
          total: {
            ...fontSize,
            color: '#a024b4',
            show: true,
            label: t('chart.weekly_goal'),
            formatter: function () {
              return (data ? data.amountUsersPaidLastWeek : 0) + '/600'
            },
          },
        },
      },
    },
  }
  const radialBarChartSeries = data
    ? [parseFloat((data.amountUsersPaidLastWeek / 6).toFixed(2))]
    : [0]

  return {
    barChartOptionsWeek,
    barChartSeriesWeek,
    donutChartOptions,
    donutChartSeries,
    radialBarChartOptions,
    radialBarChartSeries,
    isLoading,
    choosenWeekDay,
    setChoosenWeekDay,
    data,
  }
}

export default useParkingStatis
