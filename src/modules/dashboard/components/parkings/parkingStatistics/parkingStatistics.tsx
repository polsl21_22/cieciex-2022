import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import { Button, Card } from '@mui/material'

import { Parking } from 'typings'

import MapModal from '../mapModal'
import ParkingEdit from '../parkingEdit'
import useParkingStatis from './parkingStatistics.chart'
import {
  ButtonContainer,
  CardContainer,
  ChartContainer,
  Container,
  DonutChart,
  DonutContainer,
  LocalizationContainer,
  StyledChart,
  StyledChartContainer,
  ToggleButtonsContainer,
} from './parkingStatistics.style'
import ParkingStatisticsToggle from './parkingStatistics.toggle'

const ParkingStatistics = (props: { selected: Parking }) => {
  const [open, setOpen] = useState(false)
  /* eslint-disable */
  const [edit, setEdit] = useState(false)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const { selected } = props
  const {
    barChartOptionsWeek,
    donutChartOptions,
    donutChartSeries,
    radialBarChartOptions,
    radialBarChartSeries,
    isLoading,
    choosenWeekDay,
    setChoosenWeekDay,
    barChartSeriesWeek,
    data,
  } = useParkingStatis(selected)
  const { t } = useTranslation()

  return (
    <Container>
      <MapModal
        open={open}
        handleClose={handleClose}
        lng={selected.longitude}
        lat={selected.latitude}
      />
      {!isLoading &&
        (edit ? (
          <ParkingEdit selected={selected} />
        ) : (
          <DonutContainer>
            <StyledChartContainer>
              <StyledChart
                options={barChartOptionsWeek(choosenWeekDay)}
                series={barChartSeriesWeek(choosenWeekDay)}
                type="bar"
              />
              <ToggleButtonsContainer>
                <ParkingStatisticsToggle
                  choosenWeekDay={choosenWeekDay}
                  setChoosenWeekDay={setChoosenWeekDay}
                />
              </ToggleButtonsContainer>
            </StyledChartContainer>
            <ChartContainer>
              <DonutChart
                options={donutChartOptions}
                series={donutChartSeries}
                type="donut"
              />
              <DonutChart
                options={radialBarChartOptions}
                series={radialBarChartSeries}
                type="radialBar"
              />
              <CardContainer>
                <Card sx={{ backgroundColor: '#ce6fdf', padding: '10px' }}>
                  {t('card.total_reservations_created') +
                    data?.reservationsCreatedLastWeek}
                </Card>
                <Card sx={{ backgroundColor: '#ce6fdf', padding: '10px' }}>
                  {t('card.total_reservations_active') +
                    data?.reservationsCreatedLastWeek}
                </Card>
              </CardContainer>
            </ChartContainer>
            <LocalizationContainer>
              <ButtonContainer>
                <Button
                  variant="outlined"
                  color="secondary"
                  fullWidth
                  onClick={handleOpen}
                >
                  {t('buttons.map')}
                </Button>
                {/*<Button
                  variant="outlined"
                  color="secondary"
                  fullWidth
                  onClick={() => {
                    setEdit(true)
                  }}
                >
                  {t('buttons.edit')}
                </Button>*/}
              </ButtonContainer>
            </LocalizationContainer>
          </DonutContainer>
        ))}
    </Container>
  )
}
export default ParkingStatistics
