import { useTranslation } from 'react-i18next'

import { ToggleButton, ToggleButtonGroup } from '@mui/material'

import { allWeek, allWeekType } from './parkingStatistics.types'

const ParkingStatisticsToggle = (props: {
  choosenWeekDay: allWeekType
  setChoosenWeekDay: (value: allWeekType) => void
}) => {
  const { t } = useTranslation()
  const { choosenWeekDay, setChoosenWeekDay } = props
  return (
    <>
      <ToggleButtonGroup
        color="secondary"
        value={choosenWeekDay}
        exclusive
        fullWidth
        orientation="vertical"
        onChange={(_, newState) => setChoosenWeekDay(newState)}
        sx={{ backgroundColor: '#FFFFFF', marginBottom: '1vh' }}
      >
        <ToggleButton value={allWeek[0]}>{t('week.all')}</ToggleButton>
        <ToggleButton value={allWeek[2]}>{t('week.tuesday')}</ToggleButton>
        <ToggleButton value={allWeek[4]}>{t('week.thursday')}</ToggleButton>
        <ToggleButton value={allWeek[6]}>{t('week.saturday')}</ToggleButton>
      </ToggleButtonGroup>
      <ToggleButtonGroup
        color="secondary"
        value={choosenWeekDay}
        exclusive
        fullWidth
        orientation="vertical"
        onChange={(_, newState) => setChoosenWeekDay(newState)}
        sx={{ backgroundColor: '#FFFFFF', marginBottom: '1vh' }}
      >
        <ToggleButton value={allWeek[1]}>{t('week.monday')}</ToggleButton>
        <ToggleButton value={allWeek[3]}>{t('week.wednesday')}</ToggleButton>
        <ToggleButton value={allWeek[5]}>{t('week.friday')}</ToggleButton>
        <ToggleButton value={allWeek[7]}>{t('week.sunday')}</ToggleButton>
      </ToggleButtonGroup>
    </>
  )
}
export default ParkingStatisticsToggle
