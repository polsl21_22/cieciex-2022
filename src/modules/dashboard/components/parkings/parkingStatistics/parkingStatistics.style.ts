import Chart from 'react-apexcharts'

import { styled } from '@mui/material'

export const Container = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  width: '100%',
  height: '100%',
})

export const StyledChart = styled(Chart)({
  width: '70%',
})

export const ToggleButtonsContainer = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
})

export const StyledChartContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-around',
})

export const ButtonContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  width: '80%',
  gap: '5%',
})

export const DonutChart = styled(StyledChart)({
  width: '35%',
})

export const DonutContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-evenly',
  width: '100%',
  maxHeight: '90%',
})

export const ChartContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  width: '100%',
})

export const CardContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-evenly',
})

export const LocalizationContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
  width: '100%',
})

export const LabelContainer = styled('div')({
  flex: 1,
})

export const AnglesContainer = styled('div')({
  flex: 5,
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'center',
})
