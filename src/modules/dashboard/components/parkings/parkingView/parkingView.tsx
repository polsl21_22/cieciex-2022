import { useTranslation } from 'react-i18next'

import { CircularProgress } from '@mui/material'

import { logo } from 'utils'

import { Parking, Reservation } from 'typings'

import ParkingStatistics from '../parkingStatistics'
import ReservationDetails from '../reservationDetails'
import { Label, RightContainer, RightLogo } from './parkingView.style'

const ParkingView = (props: {
  reservation: Reservation | null
  parking: Parking | null
  parkings: Parking[] | undefined
}) => {
  const { reservation, parking, parkings } = props
  const { t } = useTranslation()
  return parking ? (
    reservation ? (
      <RightContainer>
        <Label>{t('page_labels.reservation_details')}</Label>
        <ReservationDetails reservation={reservation} />
      </RightContainer>
    ) : (
      <RightContainer>
        <Label>{t('page_labels.parking_details')}</Label>
        <ParkingStatistics selected={parking} />
      </RightContainer>
    )
  ) : (
    <RightContainer>
      <RightLogo src={logo.def.color}></RightLogo>
      <Label>
        {t('page_labels.parking_count')}
        {parkings ? parkings.length : <CircularProgress color="inherit" />}
      </Label>
    </RightContainer>
  )
}
export default ParkingView
