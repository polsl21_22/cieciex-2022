import { ExpandMore } from '@mui/icons-material'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
import { skipToken } from '@reduxjs/toolkit/dist/query/react'

import { useGetReservationsQuery } from 'api'

import { PageState, Parking, Reservation } from 'typings'

import { useParkingColumns } from './parkingAccordion.columns'

const ParkingAccordion = (props: {
  parking: Parking
  selectedParking: Parking | null
  handleParkingChange: (parking: Parking) => void
  reservationHandler: (reservation: Reservation) => void
  togglePageState: (newState: PageState) => void
}) => {
  const {
    parking,
    selectedParking,
    handleParkingChange,
    reservationHandler,
    togglePageState,
  } = props
  const columns = useParkingColumns()

  const { data, isLoading } = useGetReservationsQuery(
    selectedParking?.server_URL ?? skipToken
  )

  return (
    <Accordion
      expanded={selectedParking ? selectedParking.id === parking.id : false}
      onChange={() => {
        handleParkingChange(parking)
        togglePageState(PageState.parkings)
      }}
      sx={{ width: '100%' }}
    >
      <AccordionSummary expandIcon={<ExpandMore />}>
        <Typography sx={{ width: '85%', flexShrink: 0 }}>
          {parking.parking_address}
        </Typography>
      </AccordionSummary>
      <AccordionDetails>
        <DataGrid
          loading={isLoading}
          autoHeight
          onRowClick={reservation => {
            reservationHandler(reservation.row)
            togglePageState(PageState.parkings)
          }}
          columns={columns}
          rows={data?.allReservations || []}
          pageSize={10}
          rowsPerPageOptions={[10]}
        ></DataGrid>
      </AccordionDetails>
    </Accordion>
  )
}
export default ParkingAccordion
