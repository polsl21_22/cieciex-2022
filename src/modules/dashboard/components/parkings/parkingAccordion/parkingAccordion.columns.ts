import { useTranslation } from 'react-i18next';



import { GridColDef } from '@mui/x-data-grid';
import moment from 'moment';


export const useParkingColumns = () => {
  const { t } = useTranslation()
  const columns: GridColDef[] = [
    { field: 'id', headerName: 'ID', width: 60, type: 'number' },
    {
      field: 'user_id',
      headerName: t('tables.user'),
      flex: 1,
    },
    {
      field: 'plate',
      headerName: t('tables.plate'),
      flex: 1,
    },
    {
      field: 'reserved_from',
      headerName: t('tables.from'),
      valueGetter: params =>
        moment(params.row.reserved_from).format('DD/MM/YYYY HH:mm'),
      flex: 3,
    },
    {
      field: 'reserved_to',
      headerName: t('tables.to'),
      valueGetter: params =>
        moment(params.row.reserved_to).format('DD/MM/YYYY HH:mm'),
      flex: 3,
    },
  ]

  return columns
}