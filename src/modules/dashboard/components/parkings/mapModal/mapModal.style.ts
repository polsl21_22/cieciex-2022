import { styled } from '@mui/material'

export const LocationContainer = styled('div')({
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '80%',
  height: '80%',
  border: 'solid 5px #000000',
  '.leaflet-container': {
    width: '100%',
    margin: '0 auto',
    height: '100%',
  },
})
