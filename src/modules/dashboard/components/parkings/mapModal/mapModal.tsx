import { MapContainer, Marker, TileLayer } from 'react-leaflet'

import { Modal } from '@mui/material'
import L from 'leaflet'
import 'leaflet-geosearch/dist/geosearch.css'
import icon from 'leaflet/dist/images/marker-icon.png'
import iconShadow from 'leaflet/dist/images/marker-shadow.png'

import { LocationContainer } from './mapModal.style'

L.Marker.prototype.options.icon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow,
  iconAnchor: [18, 48],
})

const MapModal = (props: {
  open: boolean
  handleClose: () => void
  lat: number
  lng: number
}) => {
  const { open, handleClose, lat, lng } = props
  return (
    <Modal open={open} onClose={handleClose}>
      <LocationContainer>
        <MapContainer
          center={[lat, lng]}
          zoom={10}
          scrollWheelZoom={true}
          doubleClickZoom={false}
          attributionControl={false}
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <Marker position={{ lat: lat, lng: lng }}></Marker>
        </MapContainer>
      </LocationContainer>
    </Modal>
  )
}
export default MapModal
