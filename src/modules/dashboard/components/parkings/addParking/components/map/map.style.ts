import { styled } from "@mui/material";

export const LocationContainer = styled('div')({
  width: '100%',
  flex: 1,
  '.leaflet-container': {
    width: '100%',
    margin: '0 auto',
    height: '100%',
  },
})
