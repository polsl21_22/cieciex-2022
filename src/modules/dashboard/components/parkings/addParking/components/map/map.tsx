import { useState } from 'react'
import { UseFormSetValue } from 'react-hook-form'
import {
  MapContainer,
  Marker,
  TileLayer,
  useMapEvents,
} from 'react-leaflet'

import L from 'leaflet'
import { LatLng } from 'leaflet'
import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch'
import 'leaflet-geosearch/dist/geosearch.css'
import icon from 'leaflet/dist/images/marker-icon.png'
import iconShadow from 'leaflet/dist/images/marker-shadow.png'

import { ParkingFormFields, ParkingFormValues } from '../../addParking.services'
import { LocationContainer } from './map.style'

L.Marker.prototype.options.icon = L.icon({
  iconUrl: icon,
  shadowUrl: iconShadow,
  iconAnchor: [18, 48],
})

const Map = (props: { setValue: UseFormSetValue<ParkingFormValues> }) => {
  const [mark, setMark] = useState<LatLng | null>()
  const [clicked, setClicked] = useState(false)
  const { setValue } = props

  const setLocation = () => {
    if (mark) {
      setValue(ParkingFormFields.Latitude, mark.lat)
      setValue(ParkingFormFields.Longitude, mark.lng)
    }
  }

  const AddMarker = () => {
    const map = useMapEvents({
      click: e => {
        if (!clicked) {
          map.locate()
          const provider = new OpenStreetMapProvider()
          const searchControl = GeoSearchControl({
            provider,
            marker: {
              draggable: false,
            },
            autoClose: true,
          })
          map.addControl(searchControl)
        }
        setMark(e.latlng)
        setClicked(true)
        setLocation()
      },
      dblclick: () => {
        setMark(null)
      },
      locationfound: e => {
        map.flyTo(e.latlng, map.getZoom())
      },
    })
    return null
  }

  return (
    <LocationContainer>
      <MapContainer
        center={[80.28889, 18.67802]}
        zoom={10}
        scrollWheelZoom={true}
        doubleClickZoom={false}
        attributionControl={false}
      >
        <AddMarker />

        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {mark ? <Marker position={mark}></Marker> : <></>}
      </MapContainer>
    </LocationContainer>
  )
}
export default Map
