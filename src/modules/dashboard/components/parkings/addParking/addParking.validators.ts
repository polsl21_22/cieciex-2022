import * as yup from 'yup';


export const useParkingSchema = () => {
  const schema = yup
    .object({
      server_url: yup.string().min(2).max(32).required(),
      parking_address: yup.string().min(5).max(32).required(),
      parking_spaces: yup.number().min(1).required(),
      price_per_hour: yup.number().min(0.1).required(),
      price_per_overtime_hour: yup.number().min(0.1).required(),
    })
    .required()
  return schema
}