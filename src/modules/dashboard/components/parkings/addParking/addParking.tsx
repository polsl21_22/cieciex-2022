import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'

import { LoadingButton } from '@mui/lab'
import { Button, TextField } from '@mui/material'
import { isPending } from '@reduxjs/toolkit'
import 'leaflet/dist/leaflet.css'

import { getParkingLoadingStatus } from 'config'

import { PageState } from 'typings'

import { ParkingFormFields, useParkingFormProps } from './addParking.services'
import {
  ButtonContainer,
  Form,
  Label,
  NumberContainer,
  RightContainer,
} from './addParking.style'
import { Map } from './components'

const AddParking = (props: {
  togglePageState: (newState: PageState) => void
}) => {
  const { togglePageState } = props
  const { t } = useTranslation()
  const loading = useSelector(getParkingLoadingStatus)
  const {
    register,
    handleSubmit,
    onSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useParkingFormProps()

  const FormTextField = (props: {
    field: ParkingFormFields
    number?: boolean
  }) => {
    const { field, number } = props
    return (
      <TextField
        InputLabelProps={{ shrink: true }}
        {...register(field)}
        label={t(`labels.${field}`)}
        error={!!errors[field]}
        type={number ? 'number' : 'text'}
        disabled={isPending(loading)}
        helperText={errors[field] ? errors[field]?.message : ''}
        fullWidth
        size="small"
        color="secondary"
      />
    )
  }

  return (
    <RightContainer>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Label>{t('page_labels.add_parking')}</Label>
        <FormTextField field={ParkingFormFields.Url} />
        <FormTextField field={ParkingFormFields.Address} />
        <NumberContainer>
          <FormTextField field={ParkingFormFields.Spaces} number />
          <FormTextField field={ParkingFormFields.Price} number />
          <FormTextField field={ParkingFormFields.OvertimePrice} number />
        </NumberContainer>
        <Map setValue={setValue} />
        <ButtonContainer>
          <LoadingButton
            type="submit"
            variant="contained"
            color="secondary"
            loading={isPending(loading)}
            fullWidth
          >
            {t('buttons.add')}
          </LoadingButton>
          <Button
            variant="outlined"
            color="secondary"
            fullWidth
            disabled={isPending(loading)}
            onClick={() => {
              reset()
            }}
          >
            {t('buttons.reset')}
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            fullWidth
            disabled={isPending(loading)}
            onClick={() => {
              togglePageState(PageState.parkings)
            }}
          >
            {t('buttons.cancel')}
          </Button>
        </ButtonContainer>
      </Form>
    </RightContainer>
  )
}
export default AddParking
