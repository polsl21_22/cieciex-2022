import { styled } from '@mui/material'

export const Form = styled('form')({
  height: '90%',
  width: '80%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '3%',
})

export const Label = styled('p')({
  width: '100%',
  textAlign: 'center',
  fontSize: 'calc(min(2.5vh, 25px))',
})

export const ButtonContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  width: '100%',
  gap: '5%',
})

export const RightContainer = styled('div')({
  backgroundColor: '#FFFFFF',
  width: '100%',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  paddingTop: '2vh',
  border: '1px solid #e0e0e0',
  alignItems: 'center',
  borderRadius: '5px',
})

export const NumberContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  gap: '5%',
})