import { useForm } from 'react-hook-form'

import { yupResolver } from '@hookform/resolvers/yup'

import { addParking, useAppDispatch } from 'config'

import { useParkingSchema } from './addParking.validators'

export enum ParkingFormFields {
  Url = 'server_url',
  Address = 'parking_address',
  Spaces = 'parking_spaces',
  Price = 'price_per_hour',
  OvertimePrice = 'price_per_overtime_hour',
  Latitude = 'latitude',
  Longitude = 'longitude',
}

export interface ParkingFormValues {
  [ParkingFormFields.Url]: string
  [ParkingFormFields.Address]: string
  [ParkingFormFields.Spaces]: number
  [ParkingFormFields.Price]: number
  [ParkingFormFields.OvertimePrice]: number
  [ParkingFormFields.Latitude]: number
  [ParkingFormFields.Longitude]: number
}

export const defaultValues: ParkingFormValues = {
  [ParkingFormFields.Url]: '',
  [ParkingFormFields.Address]: '',
  [ParkingFormFields.Spaces]: 0,
  [ParkingFormFields.Price]: 0,
  [ParkingFormFields.OvertimePrice]: 0,
  [ParkingFormFields.Latitude]: 0,
  [ParkingFormFields.Longitude]: 0,
}

const useOnSubmit = (onSuccess: () => void) => {
  const dispatch = useAppDispatch()
  const onSubmit = async (data: ParkingFormValues) => {
    dispatch(addParking(data))
    onSuccess()
  }
  return onSubmit
}

export const useParkingFormProps = () => {
  const schema = useParkingSchema()
  const methods = useForm<ParkingFormValues>({
    resolver: yupResolver(schema),
    reValidateMode: 'onChange',
    defaultValues: defaultValues,
  })
  const onSubmit = useOnSubmit(methods.reset)

  return { ...methods, onSubmit }
}
