import { useTranslation } from 'react-i18next'

import moment from 'moment'

import { User, selectType } from 'typings'

import {
  Details,
  DetailsContainer,
  DetailsLabel,
  DetailsValue,
  Label,
} from './userDetails.style'

const UserDetails = (props: { selectedUser: User }) => {
  const { selectedUser } = props
  const { t } = useTranslation()
  const Detail = (props: { label: string; value: string | number }) => {
    return (
      <Details>
        <DetailsLabel>{props.label}</DetailsLabel>
        <DetailsValue>{props.value}</DetailsValue>
      </Details>
    )
  }

  return (
    <>
      <Label>{t('page_labels.user_details')}</Label>
      <DetailsContainer>
        <Detail label={t('details.id')} value={selectedUser.id} />
        <Detail label={t('details.name')} value={selectedUser.name} />
        <Detail label={t('details.surname')} value={selectedUser.surname} />
        <Detail label={t('details.login')} value={selectedUser.login} />
        <Detail label={t('details.email')} value={selectedUser.email} />
        <Detail label={t('details.phone')} value={selectedUser.phone_number} />
        <Detail
          label={t('details.created')}
          value={moment(selectedUser.created_at).format('DD/MM/YYYY HH:mm')}
        />
        <Detail
          label={t('details.type')}
          value={selectType(selectedUser.user_type)}
        />
      </DetailsContainer>
    </>
  )
}
export default UserDetails
