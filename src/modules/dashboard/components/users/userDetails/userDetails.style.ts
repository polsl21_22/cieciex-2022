import { styled } from '@mui/material';


export const DetailsContainer = styled('div')({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'start',
  minWidth: '80%',
})
export const Details = styled('div')({
  display: 'flex',
  flexDirection: 'row',
})

export const DetailsLabel = styled('p')({
  fontSize: 'calc(min(1.6vh, 16px))',
  color: '#708090',
  marginRight: '1vw',
})
export const DetailsValue = styled('p')({
  fontSize: 'calc(min(1.6vh, 16px))',
})

export const Label = styled('p')({
  width: '100%',
  textAlign: 'center',
  fontSize: 'calc(min(2.5vh, 25px))',
})