import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'

import { CircularProgress } from '@mui/material'

import { logo } from 'utils'

import { User } from 'typings'

import { Label, RightContainer, RightLogo } from './userView.style'
import ToggleEdit from '../toggleEdit'
import EditUser from '../editUser'
import UserDetails from '../userDetails'

const UserView = (props: {
  user: User | null
  isFetching: boolean
  users: User[] | undefined
}) => {
  const { user, isFetching, users } = props
  const { t } = useTranslation()

  const [editUser, setEditUser] = useState(false)

  useEffect(() => {
    if (!isFetching) setEditUser(false)
  }, [isFetching])

  return user ? (
    <>
      <ToggleEdit editUser={editUser} toggleEditUser={setEditUser} />
      <RightContainer>
        {editUser ? (
          <EditUser selectedUser={user} userLoading={isFetching} />
        ) : (
          <UserDetails selectedUser={user} />
        )}
      </RightContainer>
    </>
  ) : (
    <RightContainer>
      <RightLogo src={logo.def.color}></RightLogo>
      <Label>
        {t('page_labels.user_count')}
        {users ? users.length : <CircularProgress color="inherit" />}
      </Label>
    </RightContainer>
  )
}
export default UserView
