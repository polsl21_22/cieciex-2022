import { styled } from '@mui/material'

export const RightLogo = styled('img')({
  width: '50%',
  height: 'auto',
  margin: '10%',
})

export const RightContainer = styled('div')({
  backgroundColor: '#FFFFFF',
  width: '100%',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  paddingTop: '2vh',
  border: '1px solid #e0e0e0',
  alignItems: 'center',
  borderRadius: '5px',
})

export const Label = styled('p')({
  width: '100%',
  textAlign: 'center',
  fontSize: 'calc(min(2.5vh, 25px))',
})
