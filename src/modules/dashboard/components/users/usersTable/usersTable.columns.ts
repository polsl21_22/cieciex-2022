import { useTranslation } from 'react-i18next'

import { GridColDef } from '@mui/x-data-grid'

export const useUsersColumns = () => {
  const { t } = useTranslation()
  const columns: GridColDef[] = [
    { field: 'id', headerName: t('tables.id'), width: 50, type: 'number' },
    {
      field: 'name',
      headerName: t('tables.name'),
      flex: 1,
    },
    {
      field: 'surname',
      headerName: t('tables.surname'),
      flex: 1,
    },
    {
      field: 'login',
      headerName: t('tables.login'),
      flex: 2,
    },
  ]

  return columns
}
