import { DataGrid } from '@mui/x-data-grid'

import { User } from 'typings'

import { useUsersColumns } from './usersTable.columns'

const UsersTable = (props: {
  isLoading: boolean
  handleUserChange: (user: User) => void
  users: User[] | undefined
}) => {
  const { isLoading, handleUserChange, users } = props
  const columns = useUsersColumns()
  return (
    <DataGrid
      loading={isLoading}
      autoHeight
      sx={{ width: '100%', background: '#FFFFFF' }}
      onRowClick={user => handleUserChange(user.row)}
      columns={columns}
      pageSize={10}
      rowsPerPageOptions={[10]}
      rows={users || []}
    />
  )
}
export default UsersTable
