import { useTranslation } from 'react-i18next'

import { ToggleButton, ToggleButtonGroup } from '@mui/material'

const ToggleEdit = (props: {
  editUser: boolean
  toggleEditUser: (newState: boolean) => void
}) => {
  const { editUser, toggleEditUser } = props
  const { t } = useTranslation()
  return (
    <ToggleButtonGroup
      color="secondary"
      value={editUser}
      exclusive
      onChange={(_, newState) => toggleEditUser(newState)}
      fullWidth
      sx={{ backgroundColor: '#FFFFFF', marginBottom: '1vh' }}
    >
      <ToggleButton value={false}>{t('buttons.view')}</ToggleButton>
      <ToggleButton value={true}>{t('buttons.edit')}</ToggleButton>
    </ToggleButtonGroup>
  )
}
export default ToggleEdit
