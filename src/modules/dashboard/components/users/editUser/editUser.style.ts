import { styled } from '@mui/material'

export const Form = styled('form')({
  height: '90%',
  width: '80%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  gap: '3%',
})

export const Label = styled('p')({
  width: '100%',
  textAlign: 'center',
  fontSize: 'calc(min(2.5vh, 25px))',
})

export const ButtonContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  width: '100%',
  gap: '5%',
})
