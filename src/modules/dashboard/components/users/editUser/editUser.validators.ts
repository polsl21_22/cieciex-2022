import * as yup from 'yup'

export const useUserSchema = () => {
  const schema = yup
    .object({
      name: yup
        .string()
        .nullable()
        .transform((v, o) => (o === '' ? null : v))
        .min(2)
        .max(32),
      surname: yup
        .string()
        .nullable()
        .transform((v, o) => (o === '' ? null : v))
        .min(5)
        .max(32),
      login: yup
        .string()
        .nullable()
        .transform((v, o) => (o === '' ? null : v))
        .min(5)
        .max(32),
      password: yup
        .string()
        .nullable()
        .transform((v, o) => (o === '' ? null : v))
        .min(5)
        .max(32),
      email: yup
        .string()
        .nullable()
        .transform((v, o) => (o === '' ? null : v))
        .min(5)
        .max(32)
        .email(),
      phone_number: yup
        .string()
        .nullable()
        .transform((v, o) => (o === '' ? null : v))
        .length(9)
        .matches(/^\d+$/),
    })
    .required()
  return schema
}
