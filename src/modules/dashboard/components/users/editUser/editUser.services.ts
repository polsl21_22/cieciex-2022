import { useForm } from 'react-hook-form'

import { yupResolver } from '@hookform/resolvers/yup'

import { patchUserById, useAppDispatch } from 'config'

import { useUserSchema } from './editUser.validators'

export enum UserFormFields {
  Id = 'id',
  Name = 'name',
  Surname = 'surname',
  Login = 'login',
  Password = 'password',
  Email = 'email',
  Phone = 'phone_number',
  Type = 'user_type',
}

export interface UserFormValues {
  [UserFormFields.Id]: number
  [UserFormFields.Name]: string
  [UserFormFields.Surname]: string
  [UserFormFields.Login]: string
  [UserFormFields.Password]: string
  [UserFormFields.Email]: string
  [UserFormFields.Phone]: string
  [UserFormFields.Type]: number
}

export const defaultValues: UserFormValues = {
  [UserFormFields.Id]: 0,
  [UserFormFields.Name]: '',
  [UserFormFields.Surname]: '',
  [UserFormFields.Login]: '',
  [UserFormFields.Password]: '',
  [UserFormFields.Email]: '',
  [UserFormFields.Phone]: '',
  [UserFormFields.Type]: 0,
}

const useOnSubmit = (onSuccess: () => void) => {
  const dispatch = useAppDispatch()
  const onSubmit = async (data: UserFormValues) => {
    dispatch(patchUserById({ id: data.id, data: data }))
    onSuccess()
  }
  return onSubmit
}

export const useUserFormProps = () => {
  const schema = useUserSchema()
  const methods = useForm<UserFormValues>({
    resolver: yupResolver(schema),
    reValidateMode: 'onChange',
    defaultValues: defaultValues,
  })
  const onSubmit = useOnSubmit(methods.reset)

  return { ...methods, onSubmit }
}
