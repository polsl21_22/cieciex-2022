import { useEffect } from 'react'
import { Controller } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'

import { LoadingButton } from '@mui/lab'
import {
  Button,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
} from '@mui/material'

import { getUserLoadingStatus } from 'config'

import { User, isPending } from 'typings'

import { UserFormFields, useUserFormProps } from './editUser.services'
import { ButtonContainer, Form, Label } from './editUser.style'

const EditUser = (props: { selectedUser: User; userLoading: boolean }) => {
  const { selectedUser, userLoading } = props
  const { t } = useTranslation()
  const {
    register,
    handleSubmit,
    onSubmit,
    reset,
    formState: { errors },
    control,
  } = useUserFormProps()

  const loading = useSelector(getUserLoadingStatus)

  useEffect(() => {
    reset(selectedUser)
  }, [reset, selectedUser])

  const FormTextField = (props: { field: UserFormFields }) => {
    const { field } = props

    return (
      <TextField
        InputLabelProps={{ shrink: true }}
        {...register(field)}
        label={t(`labels.${field}`)}
        error={!!errors[field]}
        disabled={isPending(loading) || userLoading}
        helperText={errors[field] ? errors[field]?.message : ''}
        fullWidth
        size="small"
        color="secondary"
      />
    )
  }

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <Label>{t('page_labels.user_edit')}</Label>
      <FormTextField field={UserFormFields.Name} />
      <FormTextField field={UserFormFields.Surname} />
      <FormTextField field={UserFormFields.Login} />
      <FormTextField field={UserFormFields.Password} />
      <FormTextField field={UserFormFields.Email} />
      <FormTextField field={UserFormFields.Phone} />
      <Controller
        render={props => (
          <ToggleButtonGroup
            exclusive
            fullWidth
            disabled={isPending(loading) || userLoading}
            {...props.field}
            color="secondary"
            onChange={(_, value) => {
              props.field.onChange(value)
            }}
            value={props.field.value}
          >
            <ToggleButton value={1}>{t('buttons.user')}</ToggleButton>
            <ToggleButton value={2}>{t('buttons.manager')}</ToggleButton>
            <ToggleButton value={3}>{t('buttons.admin')}</ToggleButton>
          </ToggleButtonGroup>
        )}
        name={UserFormFields.Type}
        control={control}
      />
      <ButtonContainer>
        <LoadingButton
          type="submit"
          variant="contained"
          color="secondary"
          loading={isPending(loading) || userLoading}
          fullWidth
        >
          {t('buttons.edit')}
        </LoadingButton>
        <Button
          variant="outlined"
          color="secondary"
          fullWidth
          disabled={isPending(loading) || userLoading}
          onClick={() => {
            reset(selectedUser)
          }}
        >
          {t('buttons.reset')}
        </Button>
      </ButtonContainer>
    </Form>
  )
}
export default EditUser
