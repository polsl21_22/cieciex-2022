import { useTranslation } from 'react-i18next'

import { AddBox } from '@mui/icons-material'
import { IconButton, ToggleButton, ToggleButtonGroup } from '@mui/material'

import { PageState } from 'typings'

import { Container } from './toggle.style'

const Toggle = (props: {
  pageState: PageState
  togglePageState: (newState: PageState) => void
}) => {
  const { pageState, togglePageState } = props
  const { t } = useTranslation()
  return (
    <Container>
      <IconButton
        onClick={() => {
          togglePageState(PageState.addParking)
        }}
      >
        <AddBox sx={{ height: '100%' }} />
      </IconButton>
      <ToggleButtonGroup
        color="secondary"
        value={pageState}
        exclusive
        onChange={(_, newState) => togglePageState(newState)}
        fullWidth
        sx={{ backgroundColor: '#FFFFFF', marginBottom: '1vh' }}
      >
        <ToggleButton value={PageState.parkings}>
          {t('buttons.parkings')}
        </ToggleButton>
        <ToggleButton value={PageState.users}>
          {t('buttons.users')}
        </ToggleButton>
      </ToggleButtonGroup>
    </Container>
  )
}
export default Toggle
