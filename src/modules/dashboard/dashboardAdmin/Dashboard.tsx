import { Navbar } from 'components'

import { PageState } from 'typings'

import {
  AddParking,
  ParkingView,
  ParkingsTable,
  Toggle,
  UserView,
  UsersTable,
} from '../components'
import useDashboardHook from './Dashboard.hook'
import { Column, ColumnWrapper, Container, Content } from './Dashboard.style'

const Dashboard = () => {
  const {
    pageState,
    togglePageState,
    parkingsLoading,
    usersLoading,
    parkings,
    users,
    handleParkingChange,
    handleReservationChange,
    handleUserChange,
    selectedParking,
    selectedReservation,
    selectedUser,
    userFetching,
  } = useDashboardHook()

  const isUser = () => {
    return pageState === PageState.users
  }
  const isParking = () => {
    return pageState === PageState.parkings
  }

  return (
    <>
      <Navbar />
      <Container>
        <ColumnWrapper>
          <Column>
            <Toggle pageState={pageState} togglePageState={togglePageState} />
            <Content>
              {isUser() ? (
                <UsersTable
                  isLoading={usersLoading}
                  users={users}
                  handleUserChange={handleUserChange}
                />
              ) : (
                <ParkingsTable
                  isLoading={parkingsLoading}
                  parkings={parkings}
                  selected={selectedParking}
                  handler={handleParkingChange}
                  reservationHandler={handleReservationChange}
                  togglePageState={togglePageState}
                />
              )}
            </Content>
          </Column>
          <Column>
            {isUser() ? (
              <UserView
                user={selectedUser}
                isFetching={userFetching}
                users={users}
              />
            ) : isParking() ? (
              <ParkingView
                reservation={selectedReservation}
                parking={selectedParking}
                parkings={parkings}
              />
            ) : (
              <AddParking togglePageState={togglePageState} />
            )}
          </Column>
        </ColumnWrapper>
      </Container>
    </>
  )
}
export default Dashboard
