import { GridColDef } from '@mui/x-data-grid'

export const usersColumns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 50, type: 'number' },
  {
    field: 'name',
    headerName: 'Name',
    flex: 1,
  },
  {
    field: 'surname',
    headerName: 'Surname',
    flex: 1,
  },
  {
    field: 'login',
    headerName: 'Login',
    flex: 2,
  },
]
