import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'

import { skipToken } from '@reduxjs/toolkit/dist/query/react'

import { useGetParkingsQuery, useGetUserByIdQuery, useGetUsersQuery } from 'api'

import { getUserLoadingStatus } from 'config'

import { PageState, Parking, Reservation, User, isSuccess } from 'typings'

const useDashboardHook = () => {
  const [pageState, setPageState] = useState<PageState>(PageState.parkings)
  const [selectedParking, setSelectedParking] = useState<Parking | null>(null)
  const [selectedReservation, setSelectedReservation] =
    useState<Reservation | null>(null)
  const [selectedUser, setSelectedUser] = useState<User | null>(null)

  const { data: parkings, isLoading: parkingsLoading } = useGetParkingsQuery('')
  const { data: users, isLoading: usersLoading } = useGetUsersQuery('')

  const {
    data: user,
    refetch: userRefetch,
    isFetching: userFetching,
  } = useGetUserByIdQuery(selectedUser?.id ?? skipToken)

  const loading = useSelector(getUserLoadingStatus)

  useEffect(() => {
    if (isSuccess(loading)) {
      userRefetch()
    }
  }, [loading, userRefetch])

  useEffect(() => {
    setSelectedUser(user ?? null)
  }, [user, setSelectedUser])

  const togglePageState = (newState: PageState) => {
    if (newState !== null && newState !== pageState) setPageState(newState)
    if (newState === PageState.users) {
      setSelectedParking(null)
      setSelectedReservation(null)
    }
    setSelectedUser(null)
  }

  const handleParkingChange = (parking: Parking) => {
    setSelectedParking(
      selectedParking && selectedParking.id === parking.id ? null : parking
    )
    setSelectedReservation(null)
  }

  const handleUserChange = (user: User) => {
    if (selectedUser !== user) setSelectedUser(user)
  }

  const handleReservationChange = (reservation: Reservation) => {
    if (reservation !== selectedReservation) setSelectedReservation(reservation)
    else setSelectedReservation(null)
  }

  return {
    pageState,
    togglePageState,
    parkingsLoading,
    usersLoading,
    parkings,
    users,
    handleParkingChange,
    handleReservationChange,
    handleUserChange,
    selectedParking,
    selectedReservation,
    selectedUser,
    userFetching,
  }
}

export default useDashboardHook
