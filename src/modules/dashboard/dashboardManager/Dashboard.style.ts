import { styled } from '@mui/material'

export const ColumnWrapper = styled('div')({
  height: '85%',
  width: '90%',
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  gridGap: '2vw',
  
})

export const Container = styled('div')({
  width: '100vw',
  height: '93vh',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
})

export const Column = styled('div')({
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
})

export const Logo = styled('img')({
  height: '4vh',
  marginLeft: '1.5vw',
})
