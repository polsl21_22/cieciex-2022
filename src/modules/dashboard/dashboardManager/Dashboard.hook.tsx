import { useState } from 'react'

import { useGetOwnedParkingsQuery } from 'api'

import { Parking, Reservation } from 'typings'

const useDashboardHook = () => {
  const [selectedParking, setSelectedParking] = useState<Parking | null>(null)
  const [selectedReservation, setSelectedReservation] =
    useState<Reservation | null>(null)

  const { data: parkings, isLoading: parkingsLoading } =
    useGetOwnedParkingsQuery('')

  const handleParkingChange = (parking: Parking) => {
    setSelectedParking(
      selectedParking && selectedParking.id === parking.id ? null : parking
    )
    setSelectedReservation(null)
  }

  const handleReservationChange = (reservation: Reservation) => {
    if (reservation !== selectedReservation) setSelectedReservation(reservation)
    else setSelectedReservation(null)
  }

  return {
    parkingsLoading,
    parkings,
    handleParkingChange,
    handleReservationChange,
    selectedParking,
    selectedReservation,
  }
}

export default useDashboardHook
