import { AccordionManager, Navbar } from 'components'

import { ParkingView } from '../components'
import useDashboardHook from './Dashboard.hook'
import { Column, ColumnWrapper, Container } from './Dashboard.style'

const Dashboard = () => {
  const {
    parkingsLoading,
    parkings,
    handleParkingChange,
    handleReservationChange,
    selectedParking,
    selectedReservation,
  } = useDashboardHook()
  return (
    <>
      <Navbar />
      <Container>
        <ColumnWrapper>
          <Column>
            <AccordionManager
              isLoading={parkingsLoading}
              parkings={parkings}
              selected={selectedParking}
              handler={handleParkingChange}
              reservationHandler={handleReservationChange}
            />
          </Column>
          <Column>
            <ParkingView
              reservation={selectedReservation}
              parking={selectedParking}
              parkings={parkings}
            />
          </Column>
        </ColumnWrapper>
      </Container>
    </>
  )
}
export default Dashboard
