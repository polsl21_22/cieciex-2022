import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'

import { Button, TextField } from '@mui/material'

import { logo } from 'utils'

import { LanguageButton } from '../languageButton'
import { RegisterFormFields, useFormProps } from './Register.services'
import {
  ButtonContainer,
  Container,
  Form,
  FormContainer,
  Logo,
} from './Register.style'

const Register = () => {
  const {
    register,
    handleSubmit,
    onSubmit,
    formState: { errors },
  } = useFormProps()

  const { t } = useTranslation()

  const navigate = useNavigate()

  return (
    <>
      <LanguageButton />
      <Container>
        <FormContainer>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Logo src={logo.def.color} />
            <TextField
              {...register(RegisterFormFields.Name)}
              label={t('login.name')}
              error={!!errors.name}
              helperText={errors.name ? errors.name.message : ''}
              fullWidth
              color="secondary"
              size="small"
            />
            <TextField
              {...register(RegisterFormFields.Surname)}
              label={t('login.surname')}
              error={!!errors.surname}
              helperText={errors.surname ? errors.surname.message : ''}
              fullWidth
              color="secondary"
              size="small"
            />
            <TextField
              {...register(RegisterFormFields.Login)}
              label={t('login.login')}
              error={!!errors.login}
              helperText={errors.login ? errors.login.message : ''}
              fullWidth
              color="secondary"
              size="small"
            />
            <TextField
              {...register(RegisterFormFields.Email)}
              label={t('login.email')}
              error={!!errors.email}
              helperText={errors.email ? errors.email.message : ''}
              fullWidth
              color="secondary"
              size="small"
            />
            <TextField
              {...register(RegisterFormFields.Phone)}
              label={t('login.phone')}
              error={!!errors.phone_number}
              helperText={
                errors.phone_number ? errors.phone_number.message : ''
              }
              fullWidth
              color="secondary"
              size="small"
            />
            <TextField
              {...register(RegisterFormFields.Password)}
              label={t('login.password')}
              error={!!errors.password}
              helperText={errors.password ? errors.password.message : ''}
              fullWidth
              color="secondary"
              size="small"
            />
            <ButtonContainer>
              <Button
                type="submit"
                variant="contained"
                color="secondary"
                fullWidth
              >
                {t('buttons.register')}
              </Button>
              <Button
                variant="outlined"
                color="secondary"
                fullWidth
                onClick={() => {
                  navigate('/login')
                }}
              >
                {t('buttons.cancel')}
              </Button>
            </ButtonContainer>
          </Form>
        </FormContainer>
      </Container>
    </>
  )
}
export default Register
