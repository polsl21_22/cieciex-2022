import { useForm } from 'react-hook-form'

import { yupResolver } from '@hookform/resolvers/yup'

import { useAppDispatch } from 'config'
import { register } from 'config'

import { useRegisterSchema } from './Register.validator'

export enum RegisterFormFields {
  Name = 'name',
  Surname = 'surname',
  Login = 'login',
  Password = 'password',
  Email = 'email',
  Phone = 'phone_number',
}

export interface RegisterFormValues {
  [RegisterFormFields.Name]: string
  [RegisterFormFields.Surname]: string
  [RegisterFormFields.Login]: string
  [RegisterFormFields.Password]: string
  [RegisterFormFields.Email]: string
  [RegisterFormFields.Phone]: string
}

export const defaultValues: RegisterFormValues = {
  [RegisterFormFields.Name]: '',
  [RegisterFormFields.Surname]: '',
  [RegisterFormFields.Login]: '',
  [RegisterFormFields.Password]: '',
  [RegisterFormFields.Email]: '',
  [RegisterFormFields.Phone]: '',
}

const useOnSubmit = (onSuccess: () => void) => {
  const dispatch = useAppDispatch()
  const onSubmit = async (data: RegisterFormValues) => {
    dispatch(
      register({
        name: data[RegisterFormFields.Name],
        surname: data[RegisterFormFields.Surname],
        login: data[RegisterFormFields.Login],
        email: data[RegisterFormFields.Email],
        password: data[RegisterFormFields.Password],
        phone_number: data[RegisterFormFields.Phone],
      })
    )
    onSuccess()
  }
  return onSubmit
}

export const useFormProps = () => {
  const schema = useRegisterSchema()
  const methods = useForm<RegisterFormValues>({
    defaultValues: defaultValues,
    resolver: yupResolver(schema),
    reValidateMode: 'onChange',
  })
  const onSubmit = useOnSubmit(methods.reset)

  return { ...methods, onSubmit }
}
