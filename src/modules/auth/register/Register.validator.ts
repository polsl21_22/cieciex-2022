import * as yup from 'yup'

export const useRegisterSchema = () => {
  const schema = yup
    .object({
      name: yup.string().required().min(5).max(32),
      surname: yup.string().required().min(5).max(32),
      login: yup.string().required().min(5).max(32),
      password: yup.string().required().min(5).max(32),
      email: yup.string().required().email(),
      phone_number: yup.string().required().length(9).matches(/^\d+$/),
    })
    .required()
  return schema
}
