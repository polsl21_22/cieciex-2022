import { styled } from '@mui/material';


export const Container = styled('div')({
  display: 'grid',
  justifyContent: 'center',
  alignContent: 'center',
  height: '100vh',
})

export const FormContainer = styled('div')({
  border: '5px solid #9893DA',
  borderRadius: '15px',
  height: '640px',
  maxHeight: '80vh',
  width: '420px',
  maxWidth: 'calc(min(64vh, 80vw))',
  backgroundColor: '#FFFFFF',
})

export const ButtonContainer = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  width: '100%',
  gap: '5%',
})

export const Form = styled('form')({
  height: '90%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'space-around',
  margin: '5% 10%',
})

export const Logo = styled('img')({
  height: '20%',
})