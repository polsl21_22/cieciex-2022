import * as yup from 'yup'

export const useLoginSchema = () => {
  const schema = yup
    .object({
      login: yup.string().required().min(5).max(32),
      password: yup.string().required().min(5).max(32),
    })
    .required()
  return schema
}
