import { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

import { Visibility, VisibilityOff } from '@mui/icons-material'
import LoadingButton from '@mui/lab/LoadingButton'
import { Button, IconButton, InputAdornment, TextField } from '@mui/material'

import { Auth } from 'services'
import { localStorageKeys } from 'services/auth/auth'

import { logo } from 'utils'

import {
  getAuthErrorMessage,
  getAuthLoadingStatus,
  refresh,
  useAppDispatch,
} from 'config'

import { isPending } from 'typings'

import { LanguageButton } from '../languageButton'
import { LoginFormFields, useFormProps } from './Login.services'
import {
  ButtonContainer,
  Container,
  ErrorMessage,
  Form,
  FormContainer,
  Logo,
} from './Login.style'

const Login = () => {
  const { t } = useTranslation()
  const error = useSelector(getAuthErrorMessage)
  const loading = useSelector(getAuthLoadingStatus)
  const dispatch = useAppDispatch()
  const [showPassword, setShowPassword] = useState(false)
  const handleClickShowPassword = () => setShowPassword(!showPassword)
  const handleMouseDownPassword = () => setShowPassword(!showPassword)

  const {
    register,
    handleSubmit,
    onSubmit,
    formState: { errors },
  } = useFormProps()
  const navigate = useNavigate()

  useEffect(() => {
    const tryRefresh = async () => {
      const refreshToken = Auth.getItem(localStorageKeys.refreshToken)
      if (refreshToken && !Auth.isLoggedin()) {
        const res = await dispatch(refresh(refreshToken))
        if (res.meta.requestStatus === 'fulfilled') navigate('/dashboard')
      }
    }
    tryRefresh()
  }, [dispatch, navigate])

  return (
    <>
      <LanguageButton />
      <Container>
        <FormContainer>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Logo src={logo.def.color} />
            <TextField
              {...register(LoginFormFields.Login)}
              label={t('login.login')}
              error={!!errors.login}
              helperText={errors.login ? errors.login.message : ''}
              fullWidth
              color="secondary"
            />
            <TextField
              {...register(LoginFormFields.Password)}
              label={t('login.password')}
              error={!!errors.password}
              helperText={errors.password ? errors.password.message : ''}
              fullWidth
              color="secondary"
              type={showPassword ? 'text' : 'password'}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
            {error && <ErrorMessage>{error}</ErrorMessage>}
            <ButtonContainer>
              <LoadingButton
                type="submit"
                variant="contained"
                color="secondary"
                loading={isPending(loading)}
                fullWidth
              >
                {t('buttons.login')}
              </LoadingButton>
              <Button
                variant="outlined"
                color="secondary"
                fullWidth
                onClick={() => {
                  navigate('/register')
                }}
              >
                {t('buttons.register')}
              </Button>
            </ButtonContainer>
          </Form>
        </FormContainer>
      </Container>
    </>
  )
}
export default Login
