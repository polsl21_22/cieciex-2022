import { useForm } from 'react-hook-form'

import { yupResolver } from '@hookform/resolvers/yup'

import { useAppDispatch } from 'config'
import { login } from 'config'

import { useLoginSchema } from './Login.validator'

export enum LoginFormFields {
  Login = 'login',
  Password = 'password',
}

export interface LoginFormValues {
  [LoginFormFields.Login]: string
  [LoginFormFields.Password]: string
}

export const defaultValues: LoginFormValues = {
  [LoginFormFields.Login]: '',
  [LoginFormFields.Password]: '',
}

const useOnSubmit = (onSuccess: () => void) => {
  const dispatch = useAppDispatch()
  const onSubmit = async (data: LoginFormValues) => {
    dispatch(
      login({
        login: data[LoginFormFields.Login],
        password: data[LoginFormFields.Password],
      })
    )
    onSuccess()
  }
  return onSubmit
}

export const useFormProps = () => {
  const schema = useLoginSchema()
  const methods = useForm<LoginFormValues>({
    defaultValues: defaultValues,
    resolver: yupResolver(schema),
    reValidateMode: 'onChange',
  })
  const onSubmit = useOnSubmit(methods.reset)

  return { ...methods, onSubmit }
}
