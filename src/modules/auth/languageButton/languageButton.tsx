import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import { Language } from '@mui/icons-material'
import { IconButton, Menu, MenuItem, Typography } from '@mui/material'

import { Container } from './languageButton.style'

const LanguageButton = () => {
  const [anchorElLanguage, setAnchorElLanguage] = useState<null | HTMLElement>(
    null
  )

  const { t, i18n } = useTranslation()
  const handleOpenLanguageMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElLanguage(event.currentTarget)
  }

  const handleCloseLanguageMenu = () => {
    setAnchorElLanguage(null)
  }

  const languages = [
    {
      key: 1,
      label: t(`language.eng`),
      action: () => i18n.changeLanguage('eng'),
    },
    {
      key: 2,
      label: t(`language.pl`),
      action: () => i18n.changeLanguage('pl'),
    },
  ]

  return (
    <Container>
      <IconButton
        onClick={handleOpenLanguageMenu}
        sx={{ p: 0, maxWidth: '1vw', margin: '0 0.5vw' }}
      >
        <Language style={{ margin: '0 1.5vw', maxHeight: '4vh' }} />
      </IconButton>
      <Menu
        sx={{ mt: '2.5vh' }}
        anchorEl={anchorElLanguage}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorElLanguage)}
        onClose={handleCloseLanguageMenu}
      >
        {languages.map(option => (
          <MenuItem key={option.key} onClick={handleCloseLanguageMenu}>
            <Typography
              textAlign="center"
              onClick={() => {
                option.action()
              }}
            >
              {option.label}
            </Typography>
          </MenuItem>
        ))}
      </Menu>
    </Container>
  )
}
export default LanguageButton
