import { styled } from '@mui/material'

export const Container = styled('div')({
  position: 'absolute',
  top: '1vh',
  right: '1vw',
})
