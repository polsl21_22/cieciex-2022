import { Login, Refresh } from 'typings/types/auth.types'

const sec = 1000
const min = 60 * sec
const hour = 60 * min
const day = 24 * hour

const halfHour = hour / 2
const halfMounth = 14 * day

export enum localStorageKeys {
  accessToken = 'access_token',
  refreshToken = 'refresh_token',
  userId = 'user_id',
  userType = 'user_type',
}

class Auth {
  static login(credentials: Login) {
    const now = new Date()
    Auth.setItem(
      localStorageKeys.accessToken,
      credentials.accessToken,
      now.getTime() + halfHour
    )
    Auth.setItem(
      localStorageKeys.refreshToken,
      credentials.refreshToken,
      now.getTime() + halfMounth
    )
    Auth.setItem(
      localStorageKeys.userId,
      credentials.user_id.toString(),
      now.getTime() + halfMounth
    )
    Auth.setItem(
      localStorageKeys.userType,
      credentials.user_type.toString(),
      now.getTime() + halfMounth
    )
  }

  static refresh(credentials: Refresh) {
    const now = new Date()
    Auth.setItem(
      localStorageKeys.accessToken,
      credentials.accessToken,
      now.getTime() + halfHour
    )
    Auth.setItem(
      localStorageKeys.refreshToken,
      Auth.getItem(localStorageKeys.refreshToken),
      now.getTime() + halfMounth
    )
    Auth.setItem(
      localStorageKeys.userId,
      Auth.getItem(localStorageKeys.userId),
      now.getTime() + halfMounth
    )
    Auth.setItem(
      localStorageKeys.userType,
      Auth.getItem(localStorageKeys.userType),
      now.getTime() + halfMounth
    )
  }

  static getAuthorization() {
    const token = localStorage.getItem(localStorageKeys.accessToken)
    if (token === null) return ' '
    const tok = JSON.parse(token).value
    return `Bearer ${tok}`
  }

  static setItem(key: string, value: string | null, expire: number) {
    localStorage.setItem(
      key,
      JSON.stringify({
        value: value,
        expiry: expire,
      })
    )
  }

  static isOwner = () => {
    return Auth.getItem(localStorageKeys.userType) === '3'
  }

  static getItem = (key: localStorageKeys) => {
    const item = localStorage.getItem(key)
    if (item) return JSON.parse(item).value
    return null
  }

  static isLoggedin() {
    const hasEveryValue = Object.values(localStorageKeys).every(
      (value: string) => {
        return !!localStorage.getItem(value)
      }
    )
    if (hasEveryValue) return true
    return false
  }

  static removeItem(key: localStorageKeys) {
    localStorage.removeItem(key)
  }

  static Logout() {
    Object.values(localStorageKeys).forEach((value: string) => {
      localStorage.removeItem(value)
    })
  }
}
export default Auth
