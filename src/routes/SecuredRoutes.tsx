import { Navigate } from 'react-router-dom'

import { DashboardAdmin, DashboardManager } from 'modules'

import { Auth } from 'services'

const PrivateRoute = (props: { child: JSX.Element }) => {
  const { child } = props
  return Auth.isLoggedin() ? child : <Navigate to="/login" />
}

const SecuredRoutes = () => {
  const routes = [
    {
      path: '/dashboard',
      element: (
        <PrivateRoute
          child={Auth.isOwner() ? <DashboardAdmin /> : <DashboardManager />}
        />
      ),
    },
    {
      path: '*',
      element: <PrivateRoute child={<Navigate to={'/dashboard'} replace />} />,
    },
  ]
  return routes
}

export default SecuredRoutes
