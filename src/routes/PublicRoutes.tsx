import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

import { Download, Login, Register } from 'modules'

import { Auth } from 'services'

const PublicRoute = (props: { child: JSX.Element }) => {
  const { child } = props
  const navigate = useNavigate()
  useEffect(() => {
    if (Auth.isLoggedin()) navigate('/dashboard')
  }, [child, navigate])
  return child
}

const PublicRoutes = () => {
  const routes = [
    {
      path: '/login',
      element: <PublicRoute child={<Login />} />,
    },
    {
      path: '/register',
      element: <PublicRoute child={<Register />} />,
    },
    {
      path: '/download',
      element: <PublicRoute child={<Download />} />,
    },
  ]
  return routes
}

export default PublicRoutes
