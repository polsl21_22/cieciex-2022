import { useRoutes } from 'react-router-dom'

import { HistoryRouter, PublicRoutes, SecuredRoutes, history } from 'routes'

const Routes = () => {
  const routes = useRoutes(PublicRoutes().concat(SecuredRoutes()))
  return routes
}

export const Router = () => {
  return (
    <HistoryRouter history={history}>
      <Routes />
    </HistoryRouter>
  )
}
