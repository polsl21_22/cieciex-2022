import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { Auth } from 'services'

import { host } from 'config/host'

import {
  Login,
  LoginPayLoad,
  RefreshPayLoad,
  RegisterPayLoad,
} from 'typings'

export const authApi = createApi({
  reducerPath: 'auth',
  baseQuery: fetchBaseQuery({
    baseUrl: `${host}/users`,
  }),
  endpoints: builder => ({
    logIn: builder.mutation({
      query: (credentials: LoginPayLoad) => ({
        url: `/login`,
        method: 'POST',
        contentType: 'application/json',
        body: credentials,
        validateStatus: (response, result) =>
          response.status === 200 && !result.isError,
      }),
      transformResponse: (res: Login) => {
        Auth.login(res)
        return res
      },
    }),
    register: builder.mutation({
      query: (credentials: RegisterPayLoad) => ({
        url: `/register`,
        method: 'POST',
        contentType: 'application/json',
        body: credentials,
      }),
    }),
    refresh: builder.mutation({
      query: (credentials: RefreshPayLoad) => ({
        url: `/refresh`,
        method: 'POST',
        contentType: 'application/json',
        body: { refresh_token: credentials },
      }),
    }),
  }),
})

export const { useLogInMutation, useRegisterMutation, useRefreshMutation } =
  authApi
