import { createApi } from '@reduxjs/toolkit/query/react'

import { host } from 'config/host'

import { Parking, ParkingPayLoad } from 'typings'

import { baseQueryWithReauth } from './retry.api'

export const slaveApi = createApi({
  reducerPath: 'slaves',
  baseQuery: baseQueryWithReauth(`${host}/slaves`),
  endpoints: builder => ({
    getParkings: builder.query({
      query: () => ({
        url: `/`,
        method: 'GET',
      }),
      transformResponse: (response: Parking[]) => response,
    }),
    getOwnedParkings: builder.query({
      query: () => ({
        url: `/ownerParkings`,
        method: 'GET',
      }),
      transformResponse: (response: Parking[]) => response,
    }),
    addParking: builder.mutation({
      query: (data: ParkingPayLoad) => ({
        url: `/`,
        method: 'POST',
        body: data,
      }),
    }),
    getParkingById: builder.query({
      query: (id: number) => ({
        url: `/${id}`,
        method: 'GET',
      }),
      transformResponse: (response: Parking) => response,
    }),
    deleteParkingById: builder.mutation({
      query: (id: number) => ({
        url: `/${id}`,
        method: 'DELETE',
      }),
    }),
    patchParkingById: builder.mutation({
      query: ({ id, data }: { id: number; data: ParkingPayLoad }) => ({
        url: `/${id}`,
        method: 'PATCH',
        body: data,
      }),
    }),

    getoptions: builder.query({
      query: (url: string) => ({
        url: `http://api.tiles.mapbox.com/geocoding/v5/mapbox.places/${url}.json?access_token=pk.eyJ1Ijoic2x1dHNrZTIyIiwiYSI6ImNqeGw1Ym80dDAybHczdG8xaTB3aW4xMGsifQ.jCW4Xo_5khsO7VFWQz4YoQ`,
        method: 'GET',
      }),
    }),
  }),
})

export const {
  useGetParkingsQuery,
  useGetOwnedParkingsQuery,
  useAddParkingMutation,
  useGetParkingByIdQuery,
  useDeleteParkingByIdMutation,
  usePatchParkingByIdMutation,
  useGetoptionsQuery,
} = slaveApi
