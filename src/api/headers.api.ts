import { Auth } from 'services'

export const prepareAuthHeaders = async (headers: Headers) => {
  const token = Auth.getAuthorization()
  if (token) {
    headers.set('authorization', token)
  }
  return headers
}
