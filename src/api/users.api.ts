import { createApi } from '@reduxjs/toolkit/query/react'

import { host } from 'config/host'

import { User, UserPatchMePayLoad } from 'typings'

import { baseQueryWithReauth } from './retry.api'

export const userApi = createApi({
  reducerPath: 'user',
  baseQuery: baseQueryWithReauth(`${host}/users`),
  tagTypes: ['User'],
  endpoints: builder => ({
    getUsers: builder.query({
      query: () => ({
        url: `/`,
        method: 'GET',
      }),
      providesTags: ['User'],
      transformResponse: (response: User[]) => response,
    }),
    getUserById: builder.query({
      query: (id: number) => ({
        url: `/${id}`,
        method: 'GET',
      }),
      providesTags: ['User'],
      transformResponse: (response: User) => response,
    }),
    deleteUserById: builder.mutation({
      query: (id: number) => ({
        url: `/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['User'],
    }),
    patchMe: builder.mutation({
      query: ({ id, data }: { id: number; data: UserPatchMePayLoad }) => ({
        url: `/${id}`,
        method: 'PATCH',
        body: data,
      }),
      invalidatesTags: ['User'],
    }),
  }),
})

export const {
  useGetUsersQuery,
  useGetUserByIdQuery,
  useDeleteUserByIdMutation,
  usePatchMeMutation,
} = userApi
