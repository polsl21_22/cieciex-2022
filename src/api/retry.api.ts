import { fetchBaseQuery } from '@reduxjs/toolkit/query'
import type {
  BaseQueryFn,
  FetchArgs,
  FetchBaseQueryError,
} from '@reduxjs/toolkit/query'
import { Mutex } from 'async-mutex'

import { Auth } from 'services'
import { localStorageKeys } from 'services/auth/auth'

import { refresh } from 'config'

import { prepareAuthHeaders } from './headers.api'

const mutex = new Mutex()

export const baseQueryWithReauth = (url: string) => {
  const baseQuery = fetchBaseQuery({
    baseUrl: url,
    prepareHeaders: prepareAuthHeaders,
  })

  const baseQueryWithReauth: BaseQueryFn<
    string | FetchArgs,
    unknown,
    FetchBaseQueryError
  > = async (args, api, extraOptions) => {
    await mutex.waitForUnlock()
    let result = await baseQuery(args, api, extraOptions)
    if (result.error) {
      if (!mutex.isLocked()) {
        const release = await mutex.acquire()
        try {
          const refreshResult = await api.dispatch(
            refresh(Auth.getItem(localStorageKeys.refreshToken))
          )
          if (refreshResult.meta.requestStatus === 'fulfilled') {
            result = await baseQuery(args, api, extraOptions)
          }
        } finally {
          release()
        }
      } else {
        await mutex.waitForUnlock()
        result = await baseQuery(args, api, extraOptions)
      }
    }
    return result
  }
  return baseQueryWithReauth
}
