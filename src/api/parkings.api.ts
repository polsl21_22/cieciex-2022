import { createApi } from '@reduxjs/toolkit/query/react'

import { ReservationsWithStatistics } from 'typings'

import { baseQueryWithReauth } from './retry.api'

const prepareUrl = (url: string) => {
  return url.replace('http://', 'https://')
}

export const parkingsApi = createApi({
  reducerPath: 'parkings',
  baseQuery: baseQueryWithReauth(''),
  endpoints: builder => ({
    getReservations: builder.query({
      query: (url: string) => ({
        url: `${prepareUrl(url)}/reservations`,
        method: 'GET',
      }),
      transformResponse: (response: ReservationsWithStatistics) => response,
    }),
    addReservation: builder.mutation({
      query: ({
        url,
        data,
      }: {
        url: string
        data: {
          reserved_from: Date
          reserved_to: Date
          user_id: number
          plate: string
        }
      }) => ({
        url: `${prepareUrl(url)}/reservations`,
        method: 'POST',
        contentType: 'application/json',
        body: data,
      }),
    }),
  }),
})

export const { useGetReservationsQuery, useAddReservationMutation } = parkingsApi
