export interface UserPatchMePayLoad {
  name?: string
  surname?: string
  login?: string
  password?: string
  email?: string
  phone_number?: string
  user_type?: number
}