export interface ParkingPayLoad {
  server_url: string
  parking_address: string
  parking_spaces: number
  latitude: number
  longitude: number
  price_per_hour: number
  price_per_overtime_hour: number
}
