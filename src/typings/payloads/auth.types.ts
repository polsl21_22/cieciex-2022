export interface LoginPayLoad {
  login: string
  password: string
}

export interface RegisterPayLoad {
  name: string
  surname: string
  login: string
  password: string
  email: string
  phone_number: string
}

export interface RefreshPayLoad {
  refresh_token: string
}
