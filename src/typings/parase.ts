export const selectType = (type: number) => {
  switch (type) {
    case 1:
      return 'User'
    case 2:
      return 'Manager'
    case 3:
      return 'Admin'
    default:
      return 'Unknown'
  }
}
