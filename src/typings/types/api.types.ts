export interface Error {
  data: string
  error: string
  originalStatus: number
  status: string
}

export interface Response {
  data: any
  error: Error
}

export enum LoadingStatus {
  Idle = 'idle',
  Pending = 'pending',
  Succeeded = 'succeeded',
  Failed = 'failed',
}

export const isPending = (status: LoadingStatus) => {
  return status === LoadingStatus.Pending
}
export const isSuccess = (status: LoadingStatus) => {
  return status === LoadingStatus.Succeeded
}

export interface State {
  loading: LoadingStatus
  error?: string | null
}

export interface Rejected {
  error: {
    message: string
  }
  meta: {
    aborted: boolean
    arg: any
    condition: boolean
    rejectedWithValue: boolean
    requestId: string
    requestStatus: string
  }
  payload?: {
    message: string
    stack: string
  }
  type: string
}
