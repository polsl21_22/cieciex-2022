export interface Parking {
  id: number
  parking_address: string
  server_URL: string
  latitude: number
  longitude: number
  ownerId: number
  parking_spaces: number
  price_per_hour: string
  price_per_overtime_hour: string
}

export interface User {
  id: number
  created_at: Date
  name: string
  surname: string
  login: string
  email: string
  phone_number: string
  user_type: number
  servers: []
}

export interface Reservation {
  amount_paid: string
  created_at: Date
  excess_payment: string
  id: number
  is_inside: boolean
  last_left: Date
  net_received: string
  payment_intent: string
  payment_status: string
  plate: string
  receipt_URL: string
  reserved_from: Date
  reserved_to: Date
  user_id: number
}

export interface Week {
  MONDAY: number[]
  TUESDAY: number[]
  THURSDAY: number[]
  SATURDAY: number[]
  FRIDAY: number[]
  WEDNESDAY: number[]
  SUNDAY: number[]
}

export interface ReservationsWithStatistics {
  allReservations: Reservation[]
  amountUsersPaidLastWeek: number
  lastWeekStatistics: Week
  netValueReceivedLastWeek: number
  reservationsActiveLastWeek: number
  reservationsCreatedLastWeek: number
}
