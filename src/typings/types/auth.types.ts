export interface Login {
  accessToken: string
  refreshToken: string
  user_id: number
  user_type: number
}

export interface Refresh {
  accessToken: string
}
