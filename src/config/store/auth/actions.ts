import { createAsyncThunk } from '@reduxjs/toolkit'

import { authApi } from 'api'

import { history } from 'routes'

import { Auth } from 'services'

import {
  Login,
  LoginPayLoad,
  Refresh,
  RefreshPayLoad,
  RegisterPayLoad,
  Response,
} from 'typings'

export const login = createAsyncThunk(
  'auth/login',
  async (credentails: LoginPayLoad, thunkAPI) => {
    try {
      const res = (await thunkAPI.dispatch(
        authApi.endpoints.logIn.initiate(credentails)
      )) as Response
      if (res.error) throw new Error(res.error.data)
      Auth.login(res.data as Login)
      history.push('/dashboard')
    } catch (err) {
      return thunkAPI.rejectWithValue(err)
    }
  }
)

export const register = createAsyncThunk(
  'auth/register',
  async (credentails: RegisterPayLoad, thunkAPI) => {
    try {
      const res = (await thunkAPI.dispatch(
        authApi.endpoints.register.initiate(credentails)
      )) as Response
      if (res.error) throw new Error(res.error.data)
      history.push('/download', { name: credentails.name })
    } catch (err) {
      return thunkAPI.rejectWithValue(err)
    }
  }
)

export const refresh = createAsyncThunk(
  'auth/refresh',
  async (credentials: RefreshPayLoad, thunkAPI) => {
    try {
      const res = (await thunkAPI.dispatch(
        authApi.endpoints.refresh.initiate(credentials)
      )) as Response
      if (res.error) throw new Error(res.error.data)
      Auth.refresh(res.data as Refresh)
      return thunkAPI.fulfillWithValue(res.data)
    } catch (err) {
      return thunkAPI.rejectWithValue(err)
    }
  }
)
