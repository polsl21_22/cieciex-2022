import { createSelector } from '@reduxjs/toolkit'

import { RootState } from 'config'

export const getParking = (state: RootState) => state.parkingReducer

export const getParkingErrorMessage = createSelector(getParking, state => state.error)
export const getParkingLoadingStatus = createSelector(
  getParking,
  state => state.loading
)
