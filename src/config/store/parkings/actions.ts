import { createAsyncThunk } from '@reduxjs/toolkit'

import { slaveApi } from 'api'

import { ParkingPayLoad, Response } from 'typings'

export const addParking = createAsyncThunk(
  'users/patch',
  async (data: ParkingPayLoad , thunkAPI) => {
    try {
      const res = (await thunkAPI.dispatch(
        slaveApi.endpoints.addParking.initiate(data)
      )) as Response
      if (res.error) throw new Error(res.error.data)
    } catch (err) {
      return thunkAPI.rejectWithValue(err)
    }
  }
)
