import { createReducer } from '@reduxjs/toolkit'

import { LoadingStatus, Rejected, State } from 'typings'

import { addParking } from './actions'

const initialState: State = {
  loading: LoadingStatus.Idle,
  error: null,
}

export default createReducer(initialState, builder =>
  builder
    .addCase(addParking.pending, state => {
      state.loading = LoadingStatus.Pending
      state.error = initialState.error
    })
    .addCase(addParking.fulfilled, state => {
      state.loading = LoadingStatus.Succeeded
      state.error = null
    })
    .addCase(addParking.rejected, (state, action) => {
      state.loading = LoadingStatus.Failed
      const act = action as Rejected
      state.error = act.payload ? act.payload.message : act.error.message
    })
)
