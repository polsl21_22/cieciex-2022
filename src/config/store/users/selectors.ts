import { createSelector } from '@reduxjs/toolkit'

import { RootState } from 'config'

export const getUser = (state: RootState) => state.userReducer

export const getUserErrorMessage = createSelector(getUser, state => state.error)
export const getUserLoadingStatus = createSelector(getUser, state => state.loading)
