import { createAsyncThunk } from '@reduxjs/toolkit'

import { userApi } from 'api'

import { Response, UserPatchMePayLoad } from 'typings'

export const patchUserById = createAsyncThunk(
  'users/patch',
  async (payload: { id: number; data: UserPatchMePayLoad }, thunkAPI) => {
    try {
      const data = {
        name: payload.data.name ?? undefined,
        surname: payload.data.surname ?? undefined,
        login: payload.data.login ?? undefined,
        password: payload.data.password ?? undefined,
        email: payload.data.email ?? undefined,
        phone_number: payload.data.phone_number ?? undefined,
        user_type: payload.data.user_type ?? undefined,
      } as UserPatchMePayLoad
      const res = (await thunkAPI.dispatch(
        userApi.endpoints.patchMe.initiate({ id: payload.id, data: data })
      )) as Response
      if (res.error) throw new Error(res.error.data)
    } catch (err) {
      return thunkAPI.rejectWithValue(err)
    }
  }
)
