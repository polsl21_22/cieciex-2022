import { combineReducers, configureStore } from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'

import { authApi, parkingsApi, slaveApi, userApi } from 'api'

import { authReducer } from './auth'
import { authMiddleware } from './middleware'
import { parkingReducer } from './parkings'
import { userReducer } from './users'

const rootReducer = combineReducers({
  [authApi.reducerPath]: authApi.reducer,
  [userApi.reducerPath]: userApi.reducer,
  [slaveApi.reducerPath]: slaveApi.reducer,
  [parkingsApi.reducerPath]: parkingsApi.reducer,
  authReducer,
  userReducer,
  parkingReducer,
})

const setup = () => {
  const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware =>
      getDefaultMiddleware({
        serializableCheck: false,
      }).concat([
        authApi.middleware,
        userApi.middleware,
        slaveApi.middleware,
        parkingsApi.middleware,
        authMiddleware,
      ]),
  })
  setupListeners(store.dispatch)
  return store
}

export const store = setup()

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
