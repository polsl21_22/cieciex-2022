import { Middleware, isRejectedWithValue } from '@reduxjs/toolkit'

import { history } from 'routes'

import { Auth } from 'services'

export const authMiddleware: Middleware = () => next => async action => {
  if (isRejectedWithValue(action))
    if (
      action.payload.originalStatus === 401 ||
      action.payload.originalStatus === 403
    ) {
      Auth.Logout()
      history.push('/login')
    }
  return next(action)
}
