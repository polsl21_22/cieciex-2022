import { initReactI18next } from 'react-i18next'

import i18n from 'i18next'
import Backend from 'i18next-http-backend'

const DEFAULT_LANGUAGE = 'pl'

i18n
  .use(initReactI18next)
  .use(Backend)
  .init({
    lng: DEFAULT_LANGUAGE,
    fallbackLng: DEFAULT_LANGUAGE,
    interpolation: {
      escapeValue: false,
    },
    backend: {
      loadPath: '/locales/{{lng}}.json',
    },
  })

export default i18n
