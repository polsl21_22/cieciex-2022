import { useState } from 'react'
import { useTranslation } from 'react-i18next'

import { AccountCircle, Language } from '@mui/icons-material'
import { AppBar, IconButton, Menu, MenuItem, Typography } from '@mui/material'

import { history } from 'routes'

import { Auth } from 'services'

import { logo } from 'utils'

import { IconsContainer, Logo } from './Navbar.style'

const logout = () => {
  Auth.Logout()
  history.push('/login')
}

const options = [{ key: 2, label: 'Logout', action: () => logout() }]

const Navbar = () => {
  const [anchorElUser, setAnchorElUser] = useState<null | HTMLElement>(null)
  const [anchorElLanguage, setAnchorElLanguage] = useState<null | HTMLElement>(
    null
  )
  const { t, i18n } = useTranslation()

  const handleOpenLanguageMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElLanguage(event.currentTarget)
  }

  const handleCloseLanguageMenu = () => {
    setAnchorElLanguage(null)
  }
  const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElUser(event.currentTarget)
  }

  const handleCloseUserMenu = () => {
    setAnchorElUser(null)
  }

  const languages = [
    {
      key: 1,
      label: t(`language.eng`),
      action: () => i18n.changeLanguage('eng'),
    },
    {
      key: 2,
      label: t(`language.pl`),
      action: () => i18n.changeLanguage('pl'),
    },
  ]

  return (
    <AppBar
      position="static"
      color="secondary"
      sx={{
        height: '7vh',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
    >
      <Logo src={logo.oth.white}></Logo>
      <IconsContainer>
        <IconButton
          onClick={handleOpenLanguageMenu}
          sx={{ p: 0, maxWidth: '1vw', margin: '0 0.5vw' }}
        >
          <Language
            style={{ color: '#FFFFFF', margin: '0 1.5vw', maxHeight: '4vh' }}
          />
        </IconButton>
        <IconButton
          onClick={handleOpenUserMenu}
          sx={{ p: 0, maxWidth: '1vw', margin: '0 0.5vw' }}
        >
          <AccountCircle
            style={{ color: '#FFFFFF', margin: '0 1.5vw', maxHeight: '4vh' }}
          />
        </IconButton>
      </IconsContainer>
      <Menu
        sx={{ mt: '45px' }}
        anchorEl={anchorElUser}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorElUser)}
        onClose={handleCloseUserMenu}
      >
        {options.map(option => (
          <MenuItem key={option.key} onClick={handleCloseUserMenu}>
            <Typography
              textAlign="center"
              onClick={() => {
                option.action()
              }}
            >
              {option.label}
            </Typography>
          </MenuItem>
        ))}
      </Menu>
      <Menu
        sx={{ mt: '45px' }}
        anchorEl={anchorElLanguage}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorElLanguage)}
        onClose={handleCloseLanguageMenu}
      >
        {languages.map(option => (
          <MenuItem key={option.key} onClick={handleCloseLanguageMenu}>
            <Typography
              textAlign="center"
              onClick={() => {
                option.action()
              }}
            >
              {option.label}
            </Typography>
          </MenuItem>
        ))}
      </Menu>
    </AppBar>
  )
}
export default Navbar
