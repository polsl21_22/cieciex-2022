import { styled } from '@mui/material'

export const Logo = styled('img')({
  height: '4vh',
  margin: '0 1.5vw',
})

export const IconsContainer = styled('div')({
  margin: '0 1vw',
})
