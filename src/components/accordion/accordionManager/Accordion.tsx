import { ExpandMore } from '@mui/icons-material'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary, //Button,
  Typography,
} from '@mui/material'
import { DataGrid, GridColDef } from '@mui/x-data-grid'
import { skipToken } from '@reduxjs/toolkit/dist/query/react'
import moment from 'moment'

import {
  /*useAddReservationMutation,*/
  useGetReservationsQuery,
} from 'api'

//import { fakeReservation } from 'utils/fakeReservation'
import { Parking, Reservation } from 'typings'

//import { useTranslation } from 'react-i18next'

const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 50, type: 'number' },
  {
    field: 'user_id',
    headerName: 'User',
    width: 60,
  },
  {
    field: 'plate',
    headerName: 'Plate',
    width: 60,
  },
  {
    field: 'reserved_from',
    headerName: 'From',
    valueGetter: params =>
      moment(params.row.reserved_from).format('DD/MM/YYYY HH:mm'),
    flex: 1,
  },
  {
    field: 'reserved_to',
    headerName: 'To',
    valueGetter: params =>
      moment(params.row.reserved_to).format('DD/MM/YYYY HH:mm'),
    flex: 1,
  },
]

const Acc = (props: {
  parkings: Parking[] | undefined
  selected: Parking | null
  isLoading: boolean
  handler: (parking: Parking) => void
  reservationHandler: (reservation: Reservation) => void
}) => {
  const { parkings, selected, isLoading, handler, reservationHandler } = props
  /*const [addReservation] = useAddReservationMutation()
  const { t } = useTranslation()
  const generateFakeData = () => {
    if (selected)
      for (let i = 0; i < 15; i++) {
        addReservation({
          url: selected.server_URL,
          data: fakeReservation(selected.ownerId),
        })
      }
  }*/
  const { data: reservations, isLoading: reserwavionIsLoading } =
    useGetReservationsQuery(selected?.server_URL ?? skipToken)
  return (
    <>
      {!isLoading &&
        parkings &&
        parkings.map((parking: Parking) => {
          return (
            <Accordion
              key={parking.id}
              expanded={selected ? selected.id === parking.id : false}
              onChange={() => handler(parking)}
              sx={{ width: '100%' }}
            >
              <AccordionSummary expandIcon={<ExpandMore />}>
                <Typography sx={{ width: '33%', flexShrink: 0 }}>
                  {parking.parking_address}
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                {/*<Button disabled onClick={generateFakeData}>{t('buttons.fake_reservation')}</Button>*/}
                <DataGrid
                  loading={reserwavionIsLoading}
                  autoHeight
                  onRowClick={reservation => {
                    reservationHandler(reservation.row)
                  }}
                  columns={columns}
                  rows={reservations?.allReservations || []}
                  pageSize={10}
                  rowsPerPageOptions={[10]}
                ></DataGrid>
              </AccordionDetails>
            </Accordion>
          )
        })}
    </>
  )
}

export default Acc
