import React from 'react'
import { createRoot } from 'react-dom/client'
import { Provider } from 'react-redux'

import { Router } from 'routes'

import { store } from 'config'
import 'config/i18'

import './App.css'

const container = document.getElementById('root')!

export const root = createRoot(container)

root.render(
  <Provider store={store}>
    <Router />
  </Provider>
)
