export { default as black } from './apple_black.svg'
export { default as grey } from './apple_grey.svg'
export { default as white } from './apple_white.svg'
