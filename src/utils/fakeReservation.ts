import { faker } from '@faker-js/faker'
import moment from 'moment'

faker.setLocale('pl')
moment.locale('pl')

export const fakeReservation = (id: number) => {
  const start = moment()
    .add(faker.datatype.number({ min: 0, max: 0, precision: 1 }), 'days')
    .add(faker.datatype.number({ min: -6, max: 5, precision: 1 }), 'hour')
    .add(faker.datatype.number({ min: 0, max: 60, precision: 1 }), 'minute')
    .toDate()
  const end = moment(start)
    .add(faker.datatype.number({ min: 0, max: 2, precision: 1 }), 'days')
    .add(faker.datatype.number({ min: 1, max: 3, precision: 1 }), 'hour')
    .add(faker.datatype.number({ min: 0, max: 60, precision: 1 }), 'minute')
    .toDate()
  return {
    reserved_from: start,
    reserved_to: end,
    user_id: id,
    plate: faker.vehicle.vrm(),
  }
}
